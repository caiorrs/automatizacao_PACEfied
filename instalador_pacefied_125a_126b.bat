:: instalador pacefied

@echo off

echo.
echo ESTE PROGRAMA FOI FEITO COM BASE NO TUTORIAL DO TALLES E DO DEV DA PACEfied
echo.
echo.

echo AVISO LEGAL:
echo EU NAO SOU RESPONSAVEL POR QUALQUER PROBLEMA QUE VOCE VENHA A TER, COMO BRICKAR, 
echo PERDA DE DADOS, BOMBAS NUCLEARES OU QUALQUER COISA DO GENERO, FACA POR SUA CONTA!!!
echo.
echo mas estamos ae pra tirar dúvidas =P (caiorrs@gmail.com)
echo.

echo ESTE ARQUIVO SERVE PARA QUEM TEM A ROM PACEfied 1.2.5a ou 1.2.6b
echo.

echo VOCE PRECISA TER O ADB INSTALADO NO SISTEMA
echo Recomendo procurar pelo 15 second adb installer no google e instale marcando "y" em todas opções
echo TENHA PELO MENOS 80%% de BATERIA NO AMAZFIT
echo Essa eh uma medida de seguranca!!!
echo.
echo CONECTE APENAS O AMAZFIT AO PC!!!
echo.
echo.

echo Para instalar a PACEfied (Neuer_User) voce precisa ter pelo menos 700MB livres no amazfit!!!!!
echo Baixe o arquivo no link indicado (no xda rel 25 1.2.32)
echo.
echo OS PASSOS ABAIXO SAO DE EXTREMA IMPORTANCIA!!!
echo.
:: echo Deixe o arquivo na pasta Downloads!!!!
echo.
:: echo Descompacte o arquivo e bote os arquivos extraidos dentro de uma pasta com o nome:
echo.
echo Crie uma pasta com o nome PACEfiedBR em Downloads e bote TODOS OS ARQUIVOS DENTRO DELA
echo INCLUSIVE ESTE SCRIPT DEVE ESTAR DENTRO DELA, E OS ARQUIVOS PARA ROOT TAMBEM!!!
echo ARQUIVOS QUE DEVE CONTER: update.zip, md5s.txt, recovery.img, start_update.sh, boot-US-adb-root.img e boot-EN-adb-root.img
echo BAIXE TODOS OS SCRIPTS TAMBEM E BOTE-OS NA MESMA PASTA
echo.
:: echo Dentro dela devem estar os arquivos update.zip, md5s.txt, recovery.img e start_update.sh
:: echo ATENCAO, NAO BOTE ESSES ARQUIVOS EM OUTRA PASTA, ELES DEVEM ESTAR EM usuario\Downloads\PACEfiedBR
echo.

cd Downloads

IF NOT EXIST PACEfiedBR (
	echo.
	echo A PASTA PACEfiedBR NAO EXISTE!!!
	echo.
	goto fim
)

cd PACEfiedBR

IF NOT EXIST update.zip (
	echo.
	echo O arquivo update.zip não foi encontrado na pasta
	echo.
	goto fim
)

IF NOT EXIST md5s.txt (
	echo.
	echo O arquivo md5s.txt não foi encontrado na pasta
	echo.
	goto fim
)

IF NOT EXIST recovery.img (
	echo.
	echo O arquivo recovery.img não foi encontrado na pasta
	echo.
	goto fim
)

IF NOT EXIST start_update.sh (
	echo.
	echo O arquivo start_update.sh não foi encontrado na pasta
	echo.
	goto fim
)

IF NOT EXIST boot-CN-adb-root.img (
	echo.
	echo O arquivo boot-CN-adb-root.img não foi encontrado na pasta
	echo.
	goto fim
)

IF NOT EXIST boot-US-adb-root.img (
	echo.
	echo O arquivo boot-US-adb-root.img não foi encontrado na pasta
	echo.
	goto fim
)

echo O processo ira comecar
echo.
pause

adb devices

adb push update.zip /sdcard/

adb push recovery.img /sdcard/

adb push start_update.sh /sdcard/

adb push md5s.txt /sdcard/

adb shell

echo.
echo.
echo.

echo AGORA ESPERE EM TORNO DE 10 MINUTOS PARA FINALIZAR A INSTALACAO
echo.
echo QUANDO FINALIZAR, FECHE ESTA JANELA SE ELA CONTINUAR ABERTA
echo.
echo NAO INTERROMPA O PROCESSO!!!!!!

cd /sdcard/; sh start_update.sh

:fim

pause

exit