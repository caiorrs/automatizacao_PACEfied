﻿:: Instalador de apks para amazfit pace
@echo off

echo AVISO LEGAL:
echo EU NAO SOU RESPONSAVEL POR QUALQUER PROBLEMA QUE VOCE VENHA A TER, COMO BRICKAR, 
echo PERDA DE DADOS, BOMBAS NUCLEARES OU QUALQUER COISA DO GENERO, FACA POR SUA CONTA!!!
echo.
echo mas estamos ae pra tirar dúvidas =P (caiorrs@gmail.com)
echo.

echo INSTALADOR AUTOMATIZADO DE APKS PACEfied

echo Autor: Caio Roberto - @caiorrs - caiorrs@gmail.com
echo.
echo VOCE PRECISA TER O ADB INSTALADO NO SISTEMA!!!
echo se voce nao tiver, procure por 15 second adb installer no google e instale marcando "y" em todas opções

echo TENHA PELO MENOS 80%% de BATERIA NO AMAZFIT PARA CONTINUAR
echo Essa eh uma medida de seguranca!!!
echo.
echo CONECTE APENAS O AMAZFIT AO PC!!!

echo.
echo !!! ATENCAO !!! TODOS OS APKs DEVEM ESTAR NA MESMA PASTA QUE ESTE SCRIPT !!!

echo.
echo.
pause


echo ATENCAO SE VOCE FEZ O BACKUP ANTES, EH NECESSARIO RECONECTAR O AMAZFIT
echo DESCONECTE O USB E RECONECTE

echo CONTINUE PARA A INSTALACAO

pause


adb root

echo AGUARDE 3 SEGUNDOS E CONTINUE
pause


adb remount


echo AGUARDE 3 SEGUNDOS E CONTINUE
pause

:: TODOS OS APKs DEVEM ESTAR NA MESMA PASTA QUE ESSE SCRIPT

::IF NOT EXIST app (
::	echo.
::	echo NAO EXISTE A PASTA app nesse diretorio
::	echo.
::	goto pasta_priv-app
::)

:: cd app

echo. Serão instalados os apps do usuario

::APPS

IF EXIST Alipay.apk (
	echo Instalando Alipay
	echo.
	adb push Alipay.apk 			/system/app/Alipay/
	echo Alipay instalado
	echo.
)
IF EXIST AmazfitWeather.apk (
	echo Instalando AmazfitWeather
	echo.
	adb push AmazfitWeather.apk 	/system/app/AmazfitWeather/
	echo AmazfitWeather instalado
	echo.
)
IF EXIST Bluetooth.apk (
	echo Instalando Bluetooth
	echo.
	adb push Bluetooth.apk 			/system/app/Bluetooth/
	echo Bluetooth instalado
	echo.
)
IF EXIST CaptivePortalLogin.apk (
	echo Instalando CaptivePortalLogin
	echo.
	adb push CaptivePortalLogin.apk /system/app/CaptivePortalLogin/
	echo CaptivePortalLogin instalado
	echo.
)
IF EXIST CertInstaller.apk (
	echo Instalando CertInstaller
	echo.
	adb push CertInstaller.apk 		/system/app/CertInstaller/
	echo CertInstaller instalado
	echo.
)
IF EXIST ChargingUI.apk (
	echo Instalando ChargingUI
	echo.
	adb push ChargingUI.apk 		/system/app/ChargingUI/
	echo ChargingUI instalado
	echo.
)
IF EXIST HmLab.apk (
	echo Instalando HmLab
	echo.
	adb push HmLab.apk 				/system/app/HmLab/
	echo HmLab instalado
	echo.
)
IF EXIST HmMediaPlayer.apk (
	echo Instalando HmMediaPlayer
	echo.
	adb push HmMediaPlayer.apk 		/system/app/HmMediaPlayer/
	echo HmMediaPlayer instalado
	echo.
)
IF EXIST HmTvHelper.apk (
	echo Instalando HmTvHelper
	echo.
	adb push HmTvHelper.apk 		/system/app/HmTvHelper/
	echo HmTvHelper instalado
	echo.
)
IF EXIST HmVoice.apk (
	echo Instalando HmVoice
	echo.
	adb push HmVoice.apk 			/system/app/HmVoice/
	echo HmVoice instalado
	echo.
)
IF EXIST HuamiIME.apk (
	echo Instalando HuamiIME
	echo.
	adb push HuamiIME.apk 			/system/app/HuamiIME/
	echo HuamiIME instalado
	echo.
)
IF EXIST HuamiSelfTest.apk (
	echo Instalando HuamiSelfTest
	echo.
	adb push HuamiSelfTest.apk 		/system/app/HuamiSelfTest/
	echo HuamiSelfTest instalado
	echo.
)
IF EXIST HuamiWatchFaces.apk (
	echo Instalando HuamiWatchFaces
	echo.
	adb push HuamiWatchFaces.apk 	/system/app/HuamiWatchFaces/
	echo HuamiWatchFaces instalado
	echo.
)
IF EXIST KeyChain.apk (
	echo Instalando KeyChain
	echo.
	adb push KeyChain.apk 	 		/system/app/KeyChain/
	echo KeyChain instalado
	echo.
)
IF EXIST MyWatch.apk (
	echo Instalando MyWatch
	echo.
	adb push MyWatch.apk 			/system/app/MyWatch/
	echo MyWatch instalado
	echo.
)
IF EXIST PackageInstaller.apk (
	echo Instalando PackageInstaller
	echo.
	adb push PackageInstaller.apk	/system/app/PackageInstaller/
	echo PackageInstaller instalado
	echo.
)
IF EXIST SensorList.apk (
	echo Instalando SensorList
	echo.
	adb push SensorList.apk 		/system/app/SensorList/
	echo SensorList instalado
	echo.
)
IF EXIST SetupWizard.apk (
	echo Instalando SetupWizard
	echo.
	adb push SetupWizard.apk 		/system/app/SetupWizard/
	echo SetupWizard instalado
	echo.
)
IF EXIST TrainingPlan.apk (
	echo Instalando TrainingPlan
	echo.
	adb push TrainingPlan.apk 		/system/app/TrainingPlan/
	echo TrainingPlan instalado
	echo.
)
IF EXIST UnlockMIUI.apk (
	echo Instalando UnlockMIUI
	echo.
	adb push UnlockMIUI.apk 		/system/app/UnlockMIUI/
	echo UnlockMIUI instalado
	echo.
)
IF EXIST WearBLE.apk (
	echo Instalando WearBLE
	echo.
	adb push WearBLE.apk 			/system/app/WearBLE/
	echo WearBLE instalado
	echo.
)
IF EXIST WearCompass.apk (
	echo Instalando WearCompass
	echo.
	adb push WearCompass.apk 		/system/app/WearCompass/
	echo WearCompass instalado
	echo.
)
IF EXIST WearHealth.apk (
	echo Instalando WearHealth
	echo.
	adb push WearHealth.apk 		/system/app/WearHealth/
	echo WearHealth instalado
	echo.
)
IF EXIST WearHeartRate.apk (
	echo Instalando WearHeartRate
	echo.
	adb push WearHeartRate.apk 		/system/app/WearHeartRate/
	echo WearHeartRate instalado
	echo.
)
IF EXIST WearLogger.apk (
	echo Instalando WearLogger
	echo.
	adb push WearLogger.apk 		/system/app/WearLogger/
	echo WearLogger instalado
	echo.
)
IF EXIST WearSensorService.apk (
	echo Instalando WearSensorService
	echo.
	adb push WearSensorService.apk	/system/app/WearSensorService/
	echo WearSensorService instalado
	echo.
)
IF EXIST WearServices.apk (
	echo Instalando  WearServices
	echo.
	adb push WearServices.apk 		/system/app/WearServices/
	echo WearServices instalado
	echo.
)
IF EXIST WearSmartHome.apk (
	echo Instalando WearSmartHome
	echo.
	adb push WearSmartHome.apk 		/system/app/WearSmartHome/
	echo WearSmartHome instalado
	echo.
)
IF EXIST WearSports.apk (
	echo Instalando WearSports
	echo.
	adb push WearSports.apk 		/system/app/WearSports/
	echo WearSports instalado
	echo.
)
IF EXIST webview.apk (
	echo Instalando webview
	echo.
	adb push webview.apk 			/system/app/webview/
	echo webview instalado
	echo.
)
IF EXIST WeeklySports.apk (
	echo Instalando WeeklySports
	echo.
	adb push WeeklySports.apk 		/system/app/WeeklySports/
	echo WeeklySports instalado
	echo.
)
IF EXIST XimalayaSound.apk (
	echo Instalando XimalayaSound
	echo.
	adb push XimalayaSound.apk 		/system/app/XimalayaSound/
	echo XimalayaSound instalado
	echo.
)


:: PRIV-APPS

:: cd ..

::pasta_priv-app

::IF NOT EXIST priv-app (
::	echo.
::	echo NAO EXISTE A PASTA priv-app nesse diretorio
::	echo.
::	goto FIM
::)

::cd priv-app

echo.
echo Agora serao instalados os apps de sistema
echo.

IF EXIST BackupRestoreConfirmation.apk (
	echo Instalando BackupRestoreConfirmation
	echo.
	adb push BackupRestoreConfirmation.apk 		/system/priv-app/BackupRestoreConfirmation/
	echo BackupRestoreConfirmation instalado
	echo.
)
IF EXIST DefaultContainerService.apk (
	echo Instalando DefaultContainerService
	echo.
	adb push DefaultContainerService.apk		/system/priv-app/DefaultContainerService/
	echo DefaultContainerService instalado
	echo.
)
IF EXIST ExternalStorageProvider.apk (
	echo Instalando ExternalStorageProvider
	echo.
	adb push ExternalStorageProvider.apk		/system/priv-app/ExternalStorageProvider/
	echo ExternalStorageProvider instalado
	echo.
)
IF EXIST FusedLocation.apk (
	echo Instalando FusedLocation
	echo.
	adb push FusedLocation.apk 					/system/priv-app/FusedLocation/
	echo FusedLocation instalado
	echo.
)
IF EXIST HmAlarmClock.apk (
	echo Instalando HmAlarmClock
	echo.
	adb push HmAlarmClock.apk 					/system/priv-app/HmAlarmClock/
	echo HmAlarmClock instalado
	echo.
)
IF EXIST InputDevices.apk (
	echo Instalando InputDevices
	echo.
	adb push InputDevices.apk 					/system/priv-app/InputDevices/
	echo InputDevices instalado
	echo.
)
IF EXIST ManagedProvisioning.apk (
	echo Instalando ManagedProvisioning
	echo.
	adb push ManagedProvisioning.apk			/system/priv-app/ManagedProvisioning/
	echo ManagedProvisioning instalado
	echo.
)
IF EXIST MediaProvider.apk (
	echo Instalando MediaProvider
	echo.
	adb push MediaProvider.apk 					/system/priv-app/MediaProvider/	
	echo MediaProvider instalado
	echo.
)
IF EXIST OtaWatch.apk (
	echo Instalando OtaWatch
	echo.
	adb push OtaWatch.apk 						/system/priv-app/OtaWatch/
	echo OtaWatch instalado
	echo.
)
IF EXIST ProxyHandler.apk (
	echo Instalando ProxyHandler
	echo.
	adb push ProxyHandler.apk 					/system/priv-app/ProxyHandler/
	echo ProxyHandler instalado
	echo.
)
IF EXIST SettingsProvider.apk (
	echo Instalando SettingsProvider
	echo.
	adb push SettingsProvider.apk 				/system/priv-app/SettingsProvider/
	echo SettingsProvider instalado
	echo.
)
IF EXIST SharedStorageBackup.apk (
	echo Instalando SharedStorageBackup
	echo.
	adb push SharedStorageBackup.apk			/system/priv-app/SharedStorageBackup/
	echo SharedStorageBackup
	echo.
)
IF EXIST Shell.apk (
	echo Instalando Shell
	echo.
	adb push Shell.apk 							/system/priv-app/Shell/
	echo Shell instalado
	echo.
)
IF EXIST SystemUI.apk (
	echo Instalando SystemUI
	echo.
	adb push SystemUI.apk 	 					/system/priv-app/SystemUI/
	echo SystemUI instalado
	echo.
)
IF EXIST WearAirPlaneMode.apk (
	echo Instalando WearAirPlaneMode
	echo.
	adb push WearAirPlaneMode.apk				/system/priv-app/WearAirPlaneMode/
	echo WearAirPlaneMode instalado
	echo.
)
::IF EXIST WearLauncher.apk (
::	echo Instalando WearLauncher
::	echo.
::	adb push WearLauncher.apk 					/system/priv-app/WearLauncher/
::	echo WearLauncher instalado
::	echo.
::)
IF EXIST WearSettings.apk (
	echo Instalando WearSettings
	echo.
	adb push WearSettings.apk 					/system/priv-app/WearSettings/
	echo WearSettings instalado
	echo.
)
IF EXIST WifiUploadData.apk (
	echo Instalando WifiUploadData
	echo.
	adb push WifiUploadData.apk					/system/priv-app/WifiUploadData/
	echo WifiUploadData instalado
	echo.
)

echo.
echo Instalacoes Finalizadas!!
echo.
echo REINICIANDO O AMAZFIT
echo.

adb reboot

echo.

echo.

echo Se voce gostou do trabalho e deseja fazer uma doacao expontanea
echo Banco Caixa Economica Federal
echo Nome: Caio Roberto Ramos da Silva
echo Agencia: 1765
echo Conta: 00034978-4 Operação 013 (Poupanca)
echo CPF: 020.469.780-80

echo MUITO OBRIGADO
echo.

echo Por favor, se gostou do trabalho, me de feedback
echo.
echo Me ajude em https://gitlab.com/caiorrs/PACEfied_AmazFit
echo.

:FIM
pause

exit