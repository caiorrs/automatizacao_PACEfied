﻿@echo off

echo Este script serve para instalar uma nova bootanimation no amazfit PACE

echo.

echo VOCE PRECISA TER O ADB INSTALADO NO SISTEMA

echo O nome do arquivo da nova bootanimation deve ser bootanimation.zip
echo E o script PRECISA estar na mesma pasta que a bootanimation a ser instalada


echo Continuar
pause


IF NOT EXIST bootanimation.zip (
	echo O arquivo bootanimation.zip não foi encontrado
	goto fim
)

adb devices

MD backup_bootanimation_local
MD backup_bootanimation_media

cd backup_bootanimation_local

adb pull /data/local/bootanimation.zip

cd ..

cd backup_bootanimation_media

adb pull /system/media/bootanimation.zip

cd ..

echo aguarde 3 segundos
pause

adb root

echo aguarde 3 segundos
pause

adb remount

echo aguarde 3 segundos
pause
adb push bootanimation.zip /data/local/

adb push bootanimation.zip /system/media/



:fim

echo Agora o dispositivo sera reiniciado
pause
adb reboot

echo.

echo Se voce gostou do trabalho e deseja fazer uma doacao expontanea
echo Banco Caixa Economica Federal
echo Nome: Caio Roberto Ramos da Silva
echo Agencia: 1765
echo Conta: 00034978-4 Operação 013 (Poupanca)
echo CPF: 020.469.780-80

echo MUITO OBRIGADO
echo.

pause

exit