:: Desbloquear bootloader

@echo off

echo.
echo ESTE PROGRAMA FOI FEITO COM BASE NO TUTORIAL DO TALLES E DO DEV DA PACEfied
echo.
echo.

echo AVISO LEGAL:
echo EU NAO SOU RESPONSAVEL POR QUALQUER PROBLEMA QUE VOCE VENHA A TER, COMO BRICKAR, 
echo PERDA DE DADOS, BOMBAS NUCLEARES OU QUALQUER COISA DO GENERO, FACA POR SUA CONTA!!!
echo.
echo mas estamos ae pra tirar duvidas =P (caiorrs@gmail.com)
echo.

echo VOCE PRECISA TER O ADB INSTALADO NO SISTEMA
echo Recomendo procurar pelo 15 second adb installer
echo TENHA PELO MENOS 80%% de BATERIA NO AMAZFIT
echo Essa eh uma medida de seguranca!!!
echo.
echo CONECTE APENAS O AMAZFIT AO PC!!!
echo.

echo ESTE SCRIPT DEVE ESTAR JUNTO COM OS ARQUIVOS update.zip, md5s.txt, recovery.img, start_update.sh

pause
:: echo Aperte uma tecla para continuar

echo VERIFICANDO ARQUIVOS

IF NOT EXIST update.zip (
	echo.
	echo O arquivo update.zip não foi encontrado na pasta
	echo.
	goto fim
)

IF NOT EXIST md5s.txt (
	echo.
	echo O arquivo md5s.txt não foi encontrado na pasta
	echo.
	goto fim
)

IF NOT EXIST recovery.img (
	echo.
	echo O arquivo recovery.img não foi encontrado na pasta
	echo.
	goto fim
)

IF NOT EXIST start_update.sh (
	echo.
	echo O arquivo start_update.sh não foi encontrado na pasta
	echo.
	goto fim
)

echo VERIFICANDO DISPOSITIVOS CONECTADOS

adb devices

echo AGUARDE 3 SEGUNDOS E APERTE ENTER
pause

adb root 

echo AGUARDE 3 SEGUNDOS E APERTE ENTER
pause

echo Copiando arquivo update.zip
echo.
adb push update.zip /sdcard/
echo.
echo Arquivo update.zip copiado

echo.
echo Copiando arquivo recovery.img
echo.
adb push recovery.img /sdcard/
echo.
echo Arquivo recovery.img copiado

echo.
echo Copiando arquivo start_update.sh
echo.
adb push start_update.sh /sdcard/
echo.
echo Arquivo start_update.sh copiado

echo.
echo Copiando arquivo md5s.txt
echo.
adb push md5s.txt
echo.
echo Arquivo md5s.txt copiado
echo.

pause

:fim

echo.
echo.

echo Agora prossiga com a instalação abrindo o cmd (Win+R) e digitando os seguintes comandos
echo adb root
echo adb shell
echo.
echo Agora deve estar aparecendo algo com o final # (hashtag/jogo da velha/sustenido)
echo Se estiver aparecendo $ (cifrao), feche o cmd refaça
echo.
echo Próximos comandos (com hashtag aparecendo)
echo cd /sdcard/; sh start_update.sh
echo.
echo Agora espere em torno de 10 minutos para o relógio reiniciar e instalar a nova versao
echo.

pause
pause

echo.

echo Se voce gostou do trabalho e deseja fazer uma doacao expontanea
echo Banco Caixa Economica Federal
echo Nome: Caio Roberto Ramos da Silva
echo Agencia: 1765
echo Conta: 00034978-4 Operação 013 (Poupanca)
echo CPF: 020.469.780-80

echo MUITO OBRIGADO
echo.

echo FINALIZADO!!!

pause

exit